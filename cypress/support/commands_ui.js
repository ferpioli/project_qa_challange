/// <reference types="cypress" />

Cypress.Commands.add('login', () => {
  cy.visit('index.php?controller=authentication&back=my-account')
  cy.get('#email').type(Cypress.env('user_name'))
  cy.get('#passwd').type(Cypress.env('user_password'))
  cy.get('#SubmitLogin > span').click()
})

Cypress.Commands.add('logout', () => {
  cy.get('.logout').click()

})

Cypress.Commands.add('gui_sendContact', contact =>{
  cy.visit('')
  cy.get('#contact-link > a').click()
  cy.get('#id_contact').select('Webmaster')
  cy.get('#email').type(contact.email)
  cy.get('#id_order').type(contact.order)
  cy.get('#message').type(contact.message)
  cy.get('#submitMessage > span').click()
  
})


//http://automationpractice.com/index.php?controller=search&orderby=position&orderway=desc&search_query=T-Faded+Short+Sleeve&submit_search=

Cypress.Commands.add('gui_sendContact', contact =>{
  cy.visit('')
  cy.get('#contact-link > a').click()
  cy.get('#id_contact').select('Webmaster')
  cy.get('#email').type(contact.email)
  cy.get('#id_order').type(contact.order)
  cy.get('#message').type(contact.message)
  cy.get('#submitMessage > span').click()
  
})