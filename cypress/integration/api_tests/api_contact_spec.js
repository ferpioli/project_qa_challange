/// <reference  types="Cypress" />

const faker = require('faker')

describe('send contact', () =>{

  it('successfully',() =>{

    const contact = {
      email: faker.internet.email(),
      order: faker.random.uuid(),
      message: faker.random.words(5)
   
    }
    cy.api_contact_spec(contact)
    .then(response => {
      expect(response.status).to.equal(200)
     
    })

  })

})