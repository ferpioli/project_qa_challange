/// <reference types="Cypress" />

describe('logout', () => {
  beforeEach( () => cy.login())

  it('successfully', () => {
    cy.logout()

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/index.php?controller=authentication&back=my-account`)

  } )
})