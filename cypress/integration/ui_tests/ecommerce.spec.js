/// <reference types="cypress" />


describe("ecommerce", () => {
  beforeEach(() => cy.visit("http://automationpractice.com/index.php"));


  it("add specific product to cart", () => {
    cy.get('.ajax_cart_no_product').should("exist");
    cy.get('#search_query_top').type("T-Faded Short Sleeve");
    cy.get('#searchbox > .btn').click();
    cy.get('.right-block > h5 > .product-name').should("contain", "Faded Short Sleeve T-shirts")
    cy.get('.ajax_add_to_cart_button > span').click();
    cy.get('.button-container > .button-medium > span').click();
    cy.get('[title="View my shopping cart"] > .ajax_cart_quantity').should("exist");
  });

  it("researching non-existent product", () => {
    cy.get('#search_query_top').type("pants");
    cy.get('#searchbox > .btn').click();
    cy.get('.alert').should("exist");

  });

});



